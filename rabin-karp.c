#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

size_t getRabinFingerprint(const char *string)
{
	size_t size = strlen(string);
	size_t index;
	for (size_t i = 0; i < size; i++) {
		index = string[i] * (size - 1);
		size--;
	}
	return index;
}

bool search(const char *haystack, const char *needle)
{
	size_t haystackSize = strlen(haystack);
	size_t needleSize = strlen(needle);
	if (needleSize > haystackSize) {
		return false;
	}
	size_t size = haystackSize - needleSize;
	return false;
}

int main(void)
{
	char *haystack = "sumit singh";
	char *needle = "sumit";
	printf("%d", search(haystack, needle));
}
