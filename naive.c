#include <stdbool.h>
#include <stdio.h>
#include <string.h>

bool search(const char *haystack, const char *needle)
{
	size_t haystackSize = strlen(haystack);
	size_t needleSize = strlen(needle);
	if (needleSize > haystackSize) {
		return false;
	}
	size_t size = haystackSize - needleSize;
	for (size_t i = 0; i <= size; i++) {
		size_t j = 0;
		while (j < needleSize) {
			if (needle[j] != haystack[i + j]) {
				break;
			}
			j++;
		}
		if (j == needleSize) {
			return true;
		}
	}
	return false;
}

int main(void)
{
	char *haystack = "sumit singh";
	char *needle = "sumit";
	printf("%d", search(haystack, needle));
}
